package com.wapp.cqr;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.wapp.cqr.databinding.ActivityMainBinding;
import com.wapp.cqr.model.UserLogIn;
import com.wapp.cqr.utils.SharedPrefreance;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainBinding binding;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setView();
    }


    public void setView() {
        //toolbar
        setToolbar();

        binding.btnCheckIn.setOnClickListener(this);
        binding.btnHistory.setOnClickListener(this);
        binding.btnCQRCode.setOnClickListener(this);
        binding.btnAssertCode.setOnClickListener(this);
        binding.btnLogout.setOnClickListener(this);



    }


    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Salesman Check In");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnCheckIn) {
            Intent i = new Intent(MainActivity.this, GetRetInfoActivity.class);
            startActivity(i);
        } else if (view.getId() == R.id.btnHistory) {
            Intent i = new Intent(MainActivity.this, HistoryActivity.class);
            startActivity(i);
        } else if (view.getId() == R.id.btnCQRCode) {
            Intent i = new Intent(MainActivity.this, CaptureImageActivity.class);
            startActivity(i);
        } else if (view.getId() == R.id.btnAssertCode) {
           /* Intent i=new Intent(MainActivity.this,GetRetInfoActivity.class);
            startActivity(i);*/
        }
        else if (view.getId() == R.id.btn_logout){
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}
