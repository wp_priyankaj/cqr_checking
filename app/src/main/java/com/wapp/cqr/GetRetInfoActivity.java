package com.wapp.cqr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wapp.cqr.adapter.RetailerDetailsAdapter;
import com.wapp.cqr.databinding.ActivityGetRetInfoBinding;
import com.wapp.cqr.model.GetRetInfo;
import com.wapp.cqr.model.GetRetailerFence;
import com.wapp.cqr.model.RequestRetCode;
import com.wapp.cqr.model.RetailerDetailsItem;
import com.wapp.cqr.model.RetailerGeofenceItem;
import com.wapp.cqr.restclient.APIClient;
import com.wapp.cqr.restclient.RetrofitClass;
import com.wapp.cqr.utils.CommonMethod;
import com.wapp.cqr.utils.DialogActionListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRetInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private static final String TAG = "GetRetInfoActivity";
    /* access modifiers changed from: private */
    public ActivityGetRetInfoBinding binding;
    /* access modifiers changed from: private */
    public String deviceid = "357327073175247";
    private ProgressDialog mProgressDialog;
    private String retailerCode;
    /* access modifiers changed from: private */
    public RetailerGeofenceItem retailerGeofenceItem;
    /* access modifiers changed from: private */
    public String retailerId = "";
    private Toolbar toolbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.binding = (ActivityGetRetInfoBinding) DataBindingUtil.setContentView(this, R.layout.activity_get_ret_info);
        setView();
    }

    public void setView() {
        setToolbar();
        this.binding.btnNext.setBackgroundColor(getResources().getColor(R.color.inactiveBtn));
        this.binding.btnOk.setOnClickListener(this);
        this.binding.btnNext.setOnClickListener(this);
    }

    public void setToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) this.toolbar.findViewById(R.id.toolbar_title)).setText("Check In");
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setTitle((CharSequence) "");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnOk) {
            if (ActivityCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") != 0) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        ActivityCompat.requestPermissions(GetRetInfoActivity.this, new String[]{"android.permission.READ_PHONE_STATE"}, GetRetInfoActivity.PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }
                });
            } else {
                getDeviceImei();
            }
        } else if (view.getId() != R.id.btnNext) {
        } else {
            if (this.retailerId.equals("")) {
                setMsg("", "Retailer Id is null");
            } else if (!CommonMethod.checkInternetConnection(this)) {
                setMsg("No Internet connection", "You are offline Please check your internet connection.");
            }
        }
    }

    public void nextButtonClick(RetailerDetailsItem retailerDetailsItem) {
        if (retailerDetailsItem.getRetailerID() == 0) {
            setMsg("", "Retailer Id is null");
        } else if (CommonMethod.checkInternetConnection(this)) {
            getRetFence(String.valueOf(retailerDetailsItem.getRetailerID()), retailerDetailsItem.getRetailerCode());
        } else {
            setMsg("No Internet connection", "You are offline Please check your internet connection.");
        }
    }

    private void getRetFence(String str, String str2) {
        showProgressDialog();
        RequestRetCode requestRetCode = new RequestRetCode();
        requestRetCode.setActionName("GET_RET_GFENCE");
        requestRetCode.setRetailerId(str);
        requestRetCode.setRetailerCode(str2);
        requestRetCode.setMapperIMEINo(this.deviceid);
        ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).getRetFence(requestRetCode).enqueue(new Callback<GetRetailerFence>() {
            public void onResponse(Call<GetRetailerFence> call, Response<GetRetailerFence> response) {
                GetRetInfoActivity.this.hideProgressDialog();
                try {
                    Log.d(GetRetInfoActivity.TAG, "onResponse: " + response.body().getMsg() + "//" + response.body().getError());
                    if (!response.body().getError().equals("0")) {
                        GetRetInfoActivity getRetInfoActivity = GetRetInfoActivity.this;
                        getRetInfoActivity.setMsg("", response.body().getMsg() + "");
                    } else if (response.body() != null && response.body().getRetailerGeofence() != null) {
                        RetailerGeofenceItem unused = GetRetInfoActivity.this.retailerGeofenceItem = response.body().getRetailerGeofence().get(0);
                        Log.d(GetRetInfoActivity.TAG, "onResponse: " + GetRetInfoActivity.this.retailerGeofenceItem.getRetailerCode());
                        Intent intent = new Intent(GetRetInfoActivity.this, MapActivity.class);
                        intent.putExtra("IMENo", GetRetInfoActivity.this.deviceid);
                        intent.putExtra("RetailerItem", GetRetInfoActivity.this.retailerGeofenceItem);
                        GetRetInfoActivity.this.startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call<GetRetailerFence> call, Throwable th) {
                GetRetInfoActivity.this.hideProgressDialog();
                Log.d(GetRetInfoActivity.TAG, "onFailure: " + th.getMessage());
                GetRetInfoActivity getRetInfoActivity = GetRetInfoActivity.this;
                getRetInfoActivity.setMsg("", th.getMessage() + "");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == PERMISSIONS_REQUEST_READ_PHONE_STATE && iArr[0] == 0) {
            getDeviceImei();
        } else {
            Toast.makeText(this, "Permissions Deny", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void getDeviceImei() {
        this.retailerCode = this.binding.edtRetNo.getText().toString();
        CommonMethod.hideKeyboard(this);
        if (this.retailerCode.equals("")) {
            setMsg("", "Please, enter Retailer Code");
        } else if (CommonMethod.checkInternetConnection(this)) {
            showProgressDialog();
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT < 26) {
                this.deviceid = telephonyManager.getDeviceId();
            } else if (Build.VERSION.SDK_INT > 28) {
                this.deviceid = Settings.Secure.getString(getContentResolver(), "android_id");
            } else {
                this.deviceid = telephonyManager.getImei();
            }
            Log.d(NotificationCompat.CATEGORY_MESSAGE, "DeviceImei " + this.deviceid);
            if (!this.deviceid.equals("")) {
                RetInfoAPI();
            } else {
                hideProgressDialog();
            }
        } else {
            setMsg("No Internet connection", "You are offline Please check your internet connection.");
        }
    }

    private void RetInfoAPI() {
        RequestRetCode requestRetCode = new RequestRetCode();
        requestRetCode.setActionName("GET_RET_INFO");
        requestRetCode.setRetailerCode(this.retailerCode);
        requestRetCode.setMapperIMEINo(this.deviceid);
        Log.e(TAG, "RetInfoAPI: " + new Gson().toJson((Object) requestRetCode));
        ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).getRetInfo(requestRetCode).enqueue(new Callback<GetRetInfo>() {
            public void onResponse(Call<GetRetInfo> call, Response<GetRetInfo> response) {
                GetRetInfoActivity.this.hideProgressDialog();
                try {
                    Log.e(GetRetInfoActivity.TAG, "onResponse: " + new Gson().toJson((Object) response));
                    if (response.body().getErrorNo().equals("0")) {
                        GetRetInfoActivity.this.binding.tvNoData.setVisibility(View.GONE);
                        GetRetInfoActivity.this.binding.recyclerView.setVisibility(View.VISIBLE);
                        GetRetInfoActivity.this.binding.btnNext.setEnabled(true);
                        GetRetInfoActivity.this.binding.btnNext.setBackgroundColor(GetRetInfoActivity.this.getResources().getColor(R.color.colorPrimary));
                        String unused = GetRetInfoActivity.this.retailerId = response.body().getRetailerId();
                        GetRetInfoActivity.this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(GetRetInfoActivity.this));
                        RetailerDetailsAdapter retailerDetailsAdapter = new RetailerDetailsAdapter(GetRetInfoActivity.this);
                        GetRetInfoActivity.this.binding.recyclerView.setAdapter(retailerDetailsAdapter);
                        retailerDetailsAdapter.setList(response.body().getRetailerDetails());
                        return;
                    }
                    GetRetInfoActivity.this.binding.tvNoData.setVisibility(View.VISIBLE);
                    GetRetInfoActivity.this.binding.recyclerView.setVisibility(View.GONE);
                    TextView textView = GetRetInfoActivity.this.binding.tvNoData;
                    textView.setText(response.body().getMsg() + "");
                    GetRetInfoActivity.this.binding.tvNoData.setTextColor(GetRetInfoActivity.this.getResources().getColor(R.color.black));
                    GetRetInfoActivity.this.binding.btnNext.setEnabled(false);
                    GetRetInfoActivity.this.binding.btnNext.setBackgroundColor(GetRetInfoActivity.this.getResources().getColor(R.color.inactiveBtn));
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call<GetRetInfo> call, Throwable th) {
                GetRetInfoActivity.this.hideProgressDialog();
                Log.d(GetRetInfoActivity.TAG, "onFailure: " + th.getMessage());
                GetRetInfoActivity.this.setMsg("", th.getMessage());
            }
        });
    }

    public void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = CommonMethod.createProgressDialog(this);
            this.mProgressDialog.setCancelable(false);
        } else if (!isFinishing() && !this.mProgressDialog.isShowing()) {
            this.mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
    }

    public void setMsg(String str, String str2) {
        CommonMethod.singleNonCancelMessageDialog(this, str, str2, new DialogActionListener() {
            public void OnOnNegative() {
            }

            public void OnPositive() {
            }
        });
    }
}
