package com.wapp.cqr.linstner;

import android.view.View;

public interface OnRecyclerClick {
    void onClick(int pos, View v, int type);
}
