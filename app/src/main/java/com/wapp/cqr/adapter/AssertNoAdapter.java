package com.wapp.cqr.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wapp.cqr.R;
import com.wapp.cqr.model.AssetSrNosItem;

import java.util.ArrayList;
import java.util.List;

public class AssertNoAdapter extends ArrayAdapter<AssetSrNosItem> {

    public static final int TYPE_MINUS = 0;
    public static final int TYPE_PLUS = 1;
    ArrayList<AssetSrNosItem> arrayList;
    Context context;


    public AssertNoAdapter(Context context, ArrayList<AssetSrNosItem> arrayList) {
        super(context, 0, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getDropDownView(position, convertView, parent);

        return rowview(convertView, position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        return rowview(convertView, position);
    }

    private View rowview(final View convertView, int position) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.spinner_item, null);
        }
        TextView lbl = v.findViewById(R.id.tvUsers);
        lbl.setText(arrayList.get(position).getAssetSrNo());

        return v;
    }

//    @Override
//    public int getCount() {
//        return arrayList.size();
//    }

}
