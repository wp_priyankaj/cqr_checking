package com.wapp.cqr.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wapp.cqr.R;
import com.wapp.cqr.databinding.RowLayoutHistoryBinding;
import com.wapp.cqr.model.VisitHistoryItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<VisitHistoryItem> visitHistoryItems;
    private static final String TAG = "HistoryAdapter";
    private RowLayoutHistoryBinding binding;

    public HistoryAdapter(Context context, ArrayList<VisitHistoryItem> visitHistoryItems) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.visitHistoryItems = visitHistoryItems;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binding=DataBindingUtil.inflate(inflater,R.layout.row_layout_history, parent, false);
        MyViewHolder holder = new MyViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        // holder.imgIcon.setImageURI(Uri.parse(fontLists.get(position).getFontImage()));
        binding.setHistory(visitHistoryItems.get(position));

        if(binding.getHistory().getVisitDate()!=null && !binding.getHistory().getVisitDate().equals("")) {

            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(binding.getHistory().getVisitDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String formattedDate = new SimpleDateFormat("dd-MMM-yyyy").format(date);
            binding.tvDate.setText("" + formattedDate);
        }
    }

    @Override
    public int getItemCount() {
        return visitHistoryItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RowLayoutHistoryBinding binding;
        public MyViewHolder(RowLayoutHistoryBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;
        }

    }
}

