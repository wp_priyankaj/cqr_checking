package com.wapp.cqr.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wapp.cqr.GetRetInfoActivity;
import com.wapp.cqr.R;
import com.wapp.cqr.databinding.RowLayoutRetailerDetailsBinding;
import com.wapp.cqr.model.RetailerDetailsItem;

import java.util.ArrayList;
import java.util.List;

public class RetailerDetailsAdapter extends RecyclerView.Adapter<RetailerDetailsAdapter.MyViewHolder> {
    private RowLayoutRetailerDetailsBinding binding;
    /* access modifiers changed from: private */
    public Context context;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public List<RetailerDetailsItem> retailerDetailsItems = new ArrayList();

    public RetailerDetailsAdapter(Context context2) {
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.binding = (RowLayoutRetailerDetailsBinding) DataBindingUtil.inflate(this.inflater, R.layout.row_layout_retailer_details, viewGroup, false);
        return new MyViewHolder(this.binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int i) {
        this.binding.setHistory(this.retailerDetailsItems.get(i));
        this.binding.cardView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (RetailerDetailsAdapter.this.context instanceof GetRetInfoActivity) {
                    ((GetRetInfoActivity) RetailerDetailsAdapter.this.context).nextButtonClick((RetailerDetailsItem) RetailerDetailsAdapter.this.retailerDetailsItems.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.retailerDetailsItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RowLayoutRetailerDetailsBinding binding;

        public MyViewHolder(RowLayoutRetailerDetailsBinding rowLayoutRetailerDetailsBinding) {
            super(rowLayoutRetailerDetailsBinding.getRoot());
            this.binding = rowLayoutRetailerDetailsBinding;
        }
    }

    public void setList(List<RetailerDetailsItem> list) {
        this.retailerDetailsItems.clear();
        if (list != null) {
            this.retailerDetailsItems.addAll(list);
        }
        notifyDataSetChanged();
    }
}
