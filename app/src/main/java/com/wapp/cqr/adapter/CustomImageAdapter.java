package com.wapp.cqr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.wapp.cqr.R;
import com.wapp.cqr.linstner.OnRecyclerClick;
import com.wapp.cqr.model.ImagesModel;
import com.wapp.cqr.utils.Constant;
import java.util.ArrayList;

public class CustomImageAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<ImagesModel> imgList;
    private LayoutInflater inflater;
    private OnRecyclerClick onRecyclerClick;

    public CustomImageAdapter(Context context, ArrayList<ImagesModel> imgList, OnRecyclerClick onRecyclerClick) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.imgList = imgList;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = inflater.inflate(R.layout.image_rowlayout_camera, parent, false);
            MyViewHolderCamera holder = new MyViewHolderCamera(view);
            return holder;
        } else if (viewType == 1) {
            view = inflater.inflate(R.layout.image_rowlayout, parent, false);
            MyViewHolderGallery holder = new MyViewHolderGallery(view);
            return holder;
        } else {
            return null;
        }

    }


    @Override
    public int getItemViewType(int position) {
        switch (imgList.get(position).getType()) {
            case "Camera":
                return 0;
            case "Gallery":
                return 1;
            default:
                return -1;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // holder.imgIcon.setImageURI(imgList.get(position).toString());

        ImagesModel imagesModel = imgList.get(position);

        if (imagesModel.getType().equalsIgnoreCase(Constant.CameraType)) {
            //holder.imgIcon.setBackgroundResource(R.color.black);
            //holder.imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_camera));
        } else if (imagesModel.getType().equalsIgnoreCase(Constant.GalleryType)) {
            Glide.with(context).load(imagesModel.getUri())
                    .apply(RequestOptions.placeholderOf(R.mipmap.ic_launcher)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .error(R.mipmap.ic_launcher)).into(((MyViewHolderGallery) holder).imgIcon);
        }
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class MyViewHolderGallery extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon, ivDelete;

        public MyViewHolderGallery(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.image);
            ivDelete = itemView.findViewById(R.id.ivDelete);

            ivDelete.setOnClickListener(this);
            //itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.ivDelete) {
                onRecyclerClick.onClick(getAdapterPosition(), v, 0);
            }
        }
    }


    class MyViewHolderCamera extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageOpenCamera;
        RelativeLayout rvCamera;

        public MyViewHolderCamera(View itemView) {
            super(itemView);
            imageOpenCamera = itemView.findViewById(R.id.imageOpenCamera);
            rvCamera = itemView.findViewById(R.id.rvCamera);

//            imageOpenCamera.setOnClickListener(this);
//            rvCamera.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            if (v.getId() == R.id.imageOpenCamera ) {
//                {
//                    onRecyclerClick.onClick(getAdapterPosition(), v, 1);
//                }
//            }
            onRecyclerClick.onClick(getAdapterPosition(), v, 1);
        }

    }
}


