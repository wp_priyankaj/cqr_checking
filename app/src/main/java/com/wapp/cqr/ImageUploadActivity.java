package com.wapp.cqr;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;
import com.wapp.cqr.adapter.AssertNoAdapter;
import com.wapp.cqr.adapter.CustomImageAdapter;
import com.wapp.cqr.databinding.ActivityImageUploadBinding;
import com.wapp.cqr.linstner.OnRecyclerClick;
import com.wapp.cqr.model.AssetSrNosItem;
import com.wapp.cqr.model.GetAssetSrNo;
import com.wapp.cqr.model.GetImageUploadResponse;
import com.wapp.cqr.model.GetSalesManInfo;
import com.wapp.cqr.model.ImagesModel;
import com.wapp.cqr.model.RequestConfirmSManLoc;
import com.wapp.cqr.model.RequestForAssertNo;
import com.wapp.cqr.model.RequestSManLoc;
import com.wapp.cqr.restclient.APIClient;
import com.wapp.cqr.restclient.RetrofitClass;
import com.wapp.cqr.utils.CommonMethod;
import com.wapp.cqr.utils.Constant;
import com.wapp.cqr.utils.DialogActionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import gun0912.tedbottompicker.TedBottomPicker;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageUploadActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int CAMERA_PERMISSION = 200;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String TAG = "ImageUploadActivity";
    /* access modifiers changed from: private */
    public String MapperId;
    /* access modifiers changed from: private */
    public String RetailerCode;
    /* access modifiers changed from: private */
    public String RetailerID;
    double SMLat;
    double SMLong;
    /* access modifiers changed from: private */
    public String assertId = "";
    /* access modifiers changed from: private */
    public String assertNo = "";
    /* access modifiers changed from: private */
    public ArrayList<AssetSrNosItem> assetSrNosItemArrayList;
    /* access modifiers changed from: private */
    public ActivityImageUploadBinding binding;
    private String captureImage;
    double currentLat;
    double currentLong;
    private CustomImageAdapter customImageAdapter;
    private File image;
    private String imageFilePath;
    /* access modifiers changed from: private */
    public AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        public void onNothingSelected(AdapterView<?> adapterView) {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            if (i != 0) {
                ImageUploadActivity.this.binding.cvCameraClick.setVisibility(View.VISIBLE);
                String unused = ImageUploadActivity.this.assertNo = ((AssetSrNosItem) ImageUploadActivity.this.assetSrNosItemArrayList.get(i)).getAssetSrNo();
                String unused2 = ImageUploadActivity.this.assertId = String.valueOf(((AssetSrNosItem) ImageUploadActivity.this.assetSrNosItemArrayList.get(i)).getAssetID());
                return;
            }
            ImageUploadActivity.this.binding.cvCameraClick.setVisibility(View.GONE);
            String unused3 = ImageUploadActivity.this.assertNo = "";
            String unused4 = ImageUploadActivity.this.assertId = "";
        }
    };
    private ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public ArrayList<ImagesModel> selectList;
    private String stateCountry = "";
    private Toolbar toolbar;
    private Uri uri;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.binding = (ActivityImageUploadBinding) DataBindingUtil.setContentView(this, R.layout.activity_image_upload);
        setView();
    }

    public void setView() {
        setToolbar();
        this.selectList = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            this.MapperId = getIntent().getStringExtra("IMENo");
            this.RetailerID = getIntent().getStringExtra("RetailerID");
            this.RetailerCode = getIntent().getStringExtra("RetailerCode");
            this.stateCountry = getIntent().getStringExtra("stateCountry");
            this.currentLat = getIntent().getDoubleExtra("currentLat", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
            this.currentLong = getIntent().getDoubleExtra("currentLong", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
            this.SMLat = getIntent().getDoubleExtra("SMLat", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
            this.SMLong = getIntent().getDoubleExtra("SMLong", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
            Log.d(TAG, "setView: " + this.RetailerCode + " / " + this.MapperId);
            getAssetsNo(this.RetailerCode, this.MapperId);
        }
        setAdapter();
        this.binding.btnOk.setEnabled(false);
        this.binding.btnOk.setBackgroundColor(getResources().getColor(R.color.inactiveBtn));
        this.binding.btnMarkVisit.setEnabled(true);
        this.binding.btnMarkVisit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        this.binding.btnOk.setOnClickListener(this);
        this.binding.btnMarkVisit.setOnClickListener(this);
        this.binding.cvCameraClick.setOnClickListener(this);
    }

    private void getAssetsNo(String str, String str2) {
        showProgressDialog();
        this.assetSrNosItemArrayList = new ArrayList<>();
        this.assetSrNosItemArrayList.add(0, new AssetSrNosItem("Select Asset Sr Nos", 0));
        RequestForAssertNo requestForAssertNo = new RequestForAssertNo();
        requestForAssertNo.setActionName("CQR_GET_ASSTSRNO");
        requestForAssertNo.setRetailerCode(str);
        requestForAssertNo.setMapperIMEINo(str2);
        ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).getAssetSrNo(requestForAssertNo).enqueue(new Callback<GetAssetSrNo>() {
            public void onResponse(Call<GetAssetSrNo> call, Response<GetAssetSrNo> response) {
                ImageUploadActivity.this.hideProgressDialog();
                if (!response.body().getErrorNo().equals("0")) {
                    return;
                }
                if (response.body() == null || response.body().getAssetSrNos() == null) {
                    ImageUploadActivity.this.setMsg("", response.body().getMsg());
                } else if (response.body().getAssetSrNos().size() > 0) {
                    ImageUploadActivity.this.assetSrNosItemArrayList.addAll(response.body().getAssetSrNos());
                    Log.d(ImageUploadActivity.TAG, "onResponse: assetSrNosItemArrayList size: " + ImageUploadActivity.this.assetSrNosItemArrayList.size());
                    ImageUploadActivity.this.binding.spinner.setAdapter(new AssertNoAdapter(ImageUploadActivity.this, ImageUploadActivity.this.assetSrNosItemArrayList));
                    ImageUploadActivity.this.binding.spinner.setOnItemSelectedListener(ImageUploadActivity.this.listener);
                }
            }

            public void onFailure(Call<GetAssetSrNo> call, Throwable th) {
                ImageUploadActivity.this.hideProgressDialog();
                Log.d(ImageUploadActivity.TAG, "onFailure: " + th.getMessage());
                ImageUploadActivity.this.setMsg("", th.getMessage());
            }
        });
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i == 100) {
            if (iArr.length <= 0 || iArr[0] != 0 || iArr[1] != 0) {
                Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
            }
        } else if (i != 200) {
        } else {
            if (iArr.length > 0 && iArr[0] == 0 && iArr[1] == 0) {
                openCamera();
                return;
            }
            Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void setToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) this.toolbar.findViewById(R.id.toolbar_title)).setText(getString(R.string.image_upload));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setTitle((CharSequence) "");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onClick(View view) {
        if (view.getId() != R.id.btnSelectPhoto) {
            if (view.getId() == R.id.btnOk) {
                if (!CommonMethod.checkInternetConnection(this)) {
                    setMsg("No Internet connection", "You are offline Please check your internet connection.");
                } else if (this.assertId.equals("") || this.assertNo.equals("")) {
                    setMsg("", "Please select Asset Sr No");
                } else if (this.selectList.size() == 0) {
                    setMsg("", "Please capture photo.");
                } else if (this.selectList.size() > 11) {
                    setMsg("", "You can upload only 10 photos.");
                } else {
                    CommonMethod.singleMessageDialogYesNo(this, "", "Are you sure you want to upload this images?", new DialogActionListener() {
                        public void OnOnNegative() {
                        }

                        public void OnPositive() {
                            Log.d(ImageUploadActivity.TAG, "OnPositive: " + ImageUploadActivity.this.selectList.size());
                            ImageUploadActivity.this.showUriList(ImageUploadActivity.this.selectList);
                        }
                    });
                }
            } else if (view.getId() == R.id.btnMarkVisit) {
                requestForSalesManLog();
            } else if (view.getId() == R.id.btnOpenGallery) {
                if (ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") == 0 && ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                    setMultiShowButton();
                    return;
                }
                ActivityCompat.requestPermissions(this, new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"}, 100);
            } else if (view.getId() != R.id.cvCameraClick) {
            } else {
                if (ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") == 0 && ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                    openCamera();
                    return;
                }
                ActivityCompat.requestPermissions(this, new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"}, 200);
            }
        }
    }

    private void setMultiShowButton() {
        new TedBottomPicker.Builder(this).setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
            public void onImagesSelected(ArrayList<Uri> arrayList) {
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    Iterator it = ImageUploadActivity.this.selectList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        ImagesModel imagesModel = (ImagesModel) it.next();
                        if (imagesModel.getType().equals(Constant.GalleryType) && imagesModel.getUri().equals(arrayList.get(size))) {
                            arrayList.remove(size);
                            break;
                        }
                    }
                }
                Iterator<Uri> it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    Uri next = it2.next();
                    Log.d(ImageUploadActivity.TAG, "onImagesSelected: file: " + next.toString());
                    ImageUploadActivity.this.selectList.add(new ImagesModel(next, Constant.GalleryType));
                }
                if (ImageUploadActivity.this.selectList.size() > 0) {
                    ImageUploadActivity.this.binding.btnSelectPhoto.setVisibility(View.GONE);
                    ImageUploadActivity.this.binding.recyclerViewImage.setVisibility(View.VISIBLE);
                    ImageUploadActivity.this.binding.btnOk.setEnabled(true);
                    ImageUploadActivity.this.binding.btnOk.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.colorPrimary));
                    ImageUploadActivity.this.binding.btnMarkVisit.setEnabled(false);
                    ImageUploadActivity.this.binding.btnMarkVisit.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.inactiveBtn));
                }
                Log.d(ImageUploadActivity.TAG, "onImagesSelected: " + ImageUploadActivity.this.selectList.size());
                if (ImageUploadActivity.this.binding.recyclerViewImage.getAdapter() != null) {
                    ImageUploadActivity.this.binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                }
            }
        }).setPeekHeight(getResources().getDisplayMetrics().heightPixels / 2).showTitle(false).setSelectMinCount(1).setSelectMaxCount(10).setCompleteButtonText("Done").showGalleryTile(true).showCameraTile(false).setEmptySelectionText("No Select").create().show(getSupportFragmentManager());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != CAMERA_REQUEST || i2 != -1) {
            return;
        }
        if (this.image != null) {
            Log.d(TAG, "onActivityResult: RESULT_OK" + this.image.getPath());
            this.selectList.add(new ImagesModel(Uri.fromFile(this.image), Constant.GalleryType));
            if (this.selectList.size() > 0) {
                this.binding.btnSelectPhoto.setVisibility(View.GONE);
                this.binding.recyclerViewImage.setVisibility(View.VISIBLE);
                this.binding.btnOk.setEnabled(true);
                this.binding.btnOk.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                this.binding.btnMarkVisit.setEnabled(false);
                this.binding.btnMarkVisit.setBackgroundColor(getResources().getColor(R.color.inactiveBtn));
            }
            Log.d(TAG, "onImagesSelected: " + this.selectList.size());
            if (this.binding.recyclerViewImage.getAdapter() != null) {
                this.binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                return;
            }
            return;
        }
        Toast.makeText(this, "Photo capture failed", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void setAdapter() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        gridLayoutManager.setOrientation(1);
        this.binding.recyclerViewImage.setLayoutManager(gridLayoutManager);
        this.binding.recyclerViewImage.setItemAnimator(new DefaultItemAnimator());
        this.customImageAdapter = new CustomImageAdapter(this, this.selectList, new OnRecyclerClick() {
            public void onClick(int i, View view, int i2) {
                if (i2 == 1) {
                    if (ActivityCompat.checkSelfPermission(ImageUploadActivity.this, "android.permission.CAMERA") == 0 && ActivityCompat.checkSelfPermission(ImageUploadActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                        ImageUploadActivity.this.openCamera();
                        return;
                    }
                    ActivityCompat.requestPermissions(ImageUploadActivity.this, new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"}, 200);
                } else if (i2 == 0) {
                    ImageUploadActivity.this.selectList.remove(i);
                    ImageUploadActivity.this.binding.recyclerViewImage.getAdapter().notifyItemRemoved(i);
                    if (ImageUploadActivity.this.selectList.size() == 0) {
                        ImageUploadActivity.this.binding.btnSelectPhoto.setVisibility(View.VISIBLE);
                        ImageUploadActivity.this.binding.recyclerViewImage.setVisibility(View.GONE);
                        ImageUploadActivity.this.binding.btnOk.setEnabled(false);
                        ImageUploadActivity.this.binding.btnOk.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.inactiveBtn));
                        ImageUploadActivity.this.binding.btnMarkVisit.setEnabled(true);
                        ImageUploadActivity.this.binding.btnMarkVisit.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.colorPrimary));
                        return;
                    }
                    ImageUploadActivity.this.binding.btnOk.setEnabled(true);
                    ImageUploadActivity.this.binding.btnOk.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.colorPrimary));
                    ImageUploadActivity.this.binding.btnMarkVisit.setEnabled(false);
                    ImageUploadActivity.this.binding.btnMarkVisit.setBackgroundColor(ImageUploadActivity.this.getResources().getColor(R.color.inactiveBtn));
                }
            }
        });
        this.binding.recyclerViewImage.setAdapter(this.customImageAdapter);
    }

    public void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (intent.resolveActivity(getPackageManager()) != null) {
            File file = null;
            try {
                file = createImageFile1();
            } catch (IOException e) {
                Log.e(TAG, "openCamera: " + e.getMessage());
            }
            if (file != null) {
                Uri fromFile = Uri.fromFile(file);
                StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());
                intent.putExtra("output", fromFile);
                startActivityForResult(intent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile1() throws IOException {
        File file = new File(Environment.getExternalStorageDirectory(), "CQR");
        if (!file.exists()) {
            file.mkdir();
        }
        String path = file.getPath();
        this.image = new File(path, "IMG_" + new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()) + ".jpg");
        this.imageFilePath = this.image.getPath();
        Log.e(TAG, "createImageFile: " + this.imageFilePath);
        return this.image;
    }

    /* access modifiers changed from: private */
    public void showUriList(final ArrayList<ImagesModel> arrayList) {
        showProgressDialog();
        new Thread(new Runnable() {
            public void run() {
                MultipartBody.Part[] partArr = new MultipartBody.Part[arrayList.size()];
                for (int i = 0; i < arrayList.size(); i++) {
                    if (((ImagesModel) arrayList.get(i)).getType().equals(Constant.GalleryType)) {
                        try {
                            File compressToFile = new Compressor(ImageUploadActivity.this).compressToFile(new File(((ImagesModel) arrayList.get(i)).getUri().getPath()));
                            RequestBody create = RequestBody.create(MediaType.parse("image/*"), compressToFile);
                            partArr[i] = MultipartBody.Part.createFormData("image" + i, compressToFile.getName(), create);
                            Log.d(ImageUploadActivity.TAG, "showUriList: part: " + partArr[i].toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Log.e(ImageUploadActivity.TAG, "showUriList: " + partArr.length);
                Log.e(ImageUploadActivity.TAG, "showUriList: " + ImageUploadActivity.this.RetailerID + " / " + ImageUploadActivity.this.RetailerCode + " / " + ImageUploadActivity.this.MapperId + " / " + ImageUploadActivity.this.assertId + " / " + ImageUploadActivity.this.assertNo);
                ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).uploadImages(ImageUploadActivity.this.RetailerID, ImageUploadActivity.this.RetailerCode, ImageUploadActivity.this.MapperId, ImageUploadActivity.this.assertId, ImageUploadActivity.this.assertNo, ImageUploadActivity.this.binding.edtRemark.getText().toString(), ImageUploadActivity.this.binding.edtSales.getText().toString(), partArr).enqueue(new Callback<GetImageUploadResponse>() {
                    public void onResponse(Call<GetImageUploadResponse> call, Response<GetImageUploadResponse> response) {
                        ImageUploadActivity.this.hideProgressDialog();
                        Log.e(ImageUploadActivity.TAG, "onResponse: " + new Gson().toJson((Object) response));
                        if (response.body() == null) {
                            ImageUploadActivity.this.setMsg("", response.message());
                        } else if (response.body().getMsg() == null) {
                        } else {
                            if (response.body().getMsg().contains("201")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ImageUploadActivity.this);
                                View inflate = ((LayoutInflater) ImageUploadActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_success, (ViewGroup) null);
                                builder.setView(inflate);
                                builder.setCancelable(false);
                                final AlertDialog create = builder.create();
                                create.show();
                                ((TextView) inflate.findViewById(R.id.text_detail)).setText("Images uploaded successfully");
                                ((Button) inflate.findViewById(R.id.btnOk)).setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View view) {
                                        create.dismiss();
                                        ImageUploadActivity.this.selectList.clear();
                                        ImageUploadActivity.this.binding.recyclerViewImage.getAdapter().notifyDataSetChanged();
                                        Intent intent = new Intent(ImageUploadActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ImageUploadActivity.this.startActivity(intent);
                                        ImageUploadActivity.this.finish();
                                    }
                                });
                                return;
                            }
                            ImageUploadActivity.this.setMsg("", response.body().getMsg());
                        }
                    }

                    public void onFailure(Call<GetImageUploadResponse> call, Throwable th) {
                        ImageUploadActivity.this.hideProgressDialog();
                        Log.d(ImageUploadActivity.TAG, "onFailure: " + th.getMessage());
                        ImageUploadActivity.this.setMsg("", th.getMessage());
                    }
                });
            }
        }).start();
    }

    public void showProgressDialog() {
        try {
            if (this.mProgressDialog == null) {
                this.mProgressDialog = CommonMethod.createProgressDialog(this);
                this.mProgressDialog.setCancelable(false);
            } else if (!isFinishing() && !this.mProgressDialog.isShowing()) {
                this.mProgressDialog.show();
            }
        } catch (Exception unused) {
        }
    }

    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
    }

    public void setMsg(String str, String str2) {
        CommonMethod.singleNonCancelMessageDialog(this, str, str2, new DialogActionListener() {
            public void OnOnNegative() {
            }

            public void OnPositive() {
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (menuItem.getItemId() == R.id.menuHome) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public String compressImage(String str) {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        String realPathFromURI = getRealPathFromURI(str);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap decodeFile = BitmapFactory.decodeFile(realPathFromURI, options);
        int i = options.outHeight;
        int i2 = options.outWidth;
        float f = (float) (i2 / i);
        float f2 = (float) i;
        if (f2 > 816.0f || ((float) i2) > 612.0f) {
            if (f < 0.75f) {
                i2 = (int) ((816.0f / f2) * ((float) i2));
                i = (int) 816.0f;
            } else if (f > 0.75f) {
                i = (int) ((612.0f / ((float) i2)) * f2);
                i2 = (int) 612.0f;
            } else {
                i = (int) 816.0f;
                i2 = (int) 612.0f;
            }
        }
        options.inSampleSize = calculateInSampleSize(options, i2, i);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16384];
        try {
            bitmap = BitmapFactory.decodeFile(realPathFromURI, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            bitmap = decodeFile;
        }
        try {
            bitmap2 = Bitmap.createBitmap(i2, i, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            bitmap2 = null;
        }
        float f3 = (float) i2;
        float f4 = f3 / ((float) options.outWidth);
        float f5 = (float) i;
        float f6 = f5 / ((float) options.outHeight);
        float f7 = f3 / 2.0f;
        float f8 = f5 / 2.0f;
        Matrix matrix = new Matrix();
        matrix.setScale(f4, f6, f7, f8);
        Canvas canvas = new Canvas(bitmap2);
        canvas.setMatrix(matrix);
        canvas.drawBitmap(bitmap, f7 - ((float) (bitmap.getWidth() / 2)), f8 - ((float) (bitmap.getHeight() / 2)), new Paint(2));
        try {
            int attributeInt = new ExifInterface(realPathFromURI).getAttributeInt("Orientation", 0);
            Log.d("EXIF", "Exif: " + attributeInt);
            Matrix matrix2 = new Matrix();
            if (attributeInt == 6) {
                matrix2.postRotate(90.0f);
                Log.d("EXIF", "Exif: " + attributeInt);
            } else if (attributeInt == 3) {
                matrix2.postRotate(180.0f);
                Log.d("EXIF", "Exif: " + attributeInt);
            } else if (attributeInt == 8) {
                matrix2.postRotate(270.0f);
                Log.d("EXIF", "Exif: " + attributeInt);
            }
            bitmap3 = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix2, true);
        } catch (IOException e3) {
            e3.printStackTrace();
            bitmap3 = bitmap2;
        }
        String filename = getFilename();
        try {
            bitmap3.compress(Bitmap.CompressFormat.JPEG, 80, new FileOutputStream(filename));
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
        }
        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";
    }

    private String getRealPathFromURI(String str) {
        Uri parse = Uri.parse(str);
        Cursor query = getContentResolver().query(parse, (String[]) null, (String) null, (String[]) null, (String) null);
        if (query == null) {
            return parse.getPath();
        }
        query.moveToFirst();
        return query.getString(query.getColumnIndex("_data"));
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int i, int i2) {
        int i3;
        int i4 = options.outHeight;
        int i5 = options.outWidth;
        if (i4 > i2 || i5 > i) {
            i3 = Math.round(((float) i4) / ((float) i2));
            int round = Math.round(((float) i5) / ((float) i));
            if (i3 >= round) {
                i3 = round;
            }
        } else {
            i3 = 1;
        }
        while (((float) (i5 * i4)) / ((float) (i3 * i3)) > ((float) (i * i2 * 2))) {
            i3++;
        }
        return i3;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void requestForSalesManLog() {
        showProgressDialog();
        String format = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        Log.d(TAG, "requestForSalesManLog: " + this.stateCountry + " , CNFM_SMAN_LOC," + this.RetailerID + "," + format + "," + this.currentLat + "," + this.currentLong + "," + this.SMLat + "," + this.SMLong + "," + this.RetailerCode + "," + this.MapperId);
        StringBuilder sb = new StringBuilder();
        sb.append(this.stateCountry);
        sb.append("#MarkedBySelf");
        ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).getSalesManLog(new RequestSManLoc(sb.toString(), "CNFM_SMAN_LOC", this.RetailerID, format, this.currentLat, this.currentLong, this.SMLat, this.SMLong, this.RetailerCode, this.MapperId, this.binding.edtRemark.getText().toString(), this.binding.edtSales.getText().toString())).enqueue(new Callback<GetSalesManInfo>() {
            public void onResponse(Call<GetSalesManInfo> call, Response<GetSalesManInfo> response) {
                ImageUploadActivity.this.hideProgressDialog();
                if (response.body().getErrorNo().equals("0")) {
                    ImageUploadActivity imageUploadActivity = ImageUploadActivity.this;
                    Toast.makeText(imageUploadActivity, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ImageUploadActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    ImageUploadActivity.this.startActivity(intent);
                    ImageUploadActivity.this.finish();
                    return;
                }
                ImageUploadActivity.this.setMsg("", response.body().getMsg());
            }

            public void onFailure(Call<GetSalesManInfo> call, Throwable th) {
                ImageUploadActivity.this.hideProgressDialog();
                Log.d(ImageUploadActivity.TAG, "onFailure: " + th.getMessage());
                ImageUploadActivity.this.setMsg("", th.getMessage());
            }
        });
    }
}
