package com.wapp.cqr;

import android.Manifest;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.arsy.maps_library.MapRipple;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wapp.cqr.databinding.ActivityMapBinding;
import com.wapp.cqr.model.GetSalesManInfo;
import com.wapp.cqr.model.RequestSManLoc;
import com.wapp.cqr.model.RetailerGeofenceItem;
import com.wapp.cqr.restclient.APIClient;
import com.wapp.cqr.restclient.RetrofitClass;
import com.wapp.cqr.utils.CommonMethod;
import com.wapp.cqr.utils.DialogActionListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, View.OnClickListener {
    private static final int PERMISSIONS_REQUEST_STATE = 999;
    static final int REQUEST_LOCATION = 199;
    private static final String TAG = "MapActivity";
    private boolean GPSEnabled = false;
    /* access modifiers changed from: private */
    public String MapperId;
    /* access modifiers changed from: private */
    public double SMLat;
    /* access modifiers changed from: private */
    public double SMLong;
    private String actionName = "CNFM_SMAN_LOC";
    private ActivityMapBinding binding;
    private CircleOptions circleOptions;
    /* access modifiers changed from: private */
    public double currentLat;
    /* access modifiers changed from: private */
    public double currentLong;
    private LocationManager locationManager;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationRequest mLocationRequest;
    private ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public MapRipple mapRipple;
    private int radius = 100;
    private PendingResult<LocationSettingsResult> result;
    /* access modifiers changed from: private */
    public RetailerGeofenceItem retailerGeofenceItem;
    /* access modifiers changed from: private */
    public String stateCountry = "";
    ValueAnimator vAnimator;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.binding = (ActivityMapBinding) DataBindingUtil.setContentView(this, R.layout.activity_map);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(512, 512);
        }
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 && ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            setView();
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, PERMISSIONS_REQUEST_STATE);
    }

    public void setView() {
        if (getIntent().getExtras() != null) {
            this.retailerGeofenceItem = (RetailerGeofenceItem) getIntent().getSerializableExtra("RetailerItem");
            this.MapperId = getIntent().getStringExtra("IMENo");
            this.SMLat = this.retailerGeofenceItem.getLatitude().doubleValue();
            this.SMLong = this.retailerGeofenceItem.getLongitude().doubleValue();
            this.radius = this.retailerGeofenceItem.getRadius();
        }
        this.binding.btnNext.setOnClickListener(this);
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == PERMISSIONS_REQUEST_STATE && iArr[0] == 0 && iArr[1] == 0) {
            setView();
            return;
        }
        Toast.makeText(this, "Permissions Deny", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(this.SMLat, this.SMLong);
        initRadarNew(googleMap, latLng);
        googleMap.addMarker(new MarkerOptions().position(latLng).title(""));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(17.0f));
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            googleMap.setMyLocationEnabled(true);
            this.circleOptions = new CircleOptions();
            this.circleOptions.center(new LatLng(this.SMLat, this.SMLong));
            this.circleOptions.radius((double) this.radius);
            this.circleOptions.strokeColor(0);
            googleMap.addCircle(this.circleOptions);
        }
    }

    private void initRadar(GoogleMap googleMap, LatLng latLng) {
        this.mapRipple = new MapRipple(googleMap, new LatLng(latLng.latitude, latLng.longitude), this);
        this.mapRipple.withDistance((double) this.radius);
        this.mapRipple.withNumberOfRipples(3);
        this.mapRipple.withFillColor(getResources().getColor(R.color.mapRipple));
        this.mapRipple.withStrokeColor(getResources().getColor(R.color.mapRipple));
        this.mapRipple.withStrokewidth(10);
        this.mapRipple.withRippleDuration(12000);
        this.mapRipple.withTransparency(0.5f);
        this.mapRipple.startRippleMapAnimation();
    }

    private void initRadarNew(final GoogleMap googleMap, final LatLng latLng) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MapActivity.this.OverLay(googleMap.addGroundOverlay(new GroundOverlayOptions().position(latLng, 2000.0f).transparency(0.5f).image(BitmapDescriptorFactory.fromBitmap(MapActivity.this.getBitmap(R.drawable.ripple_circle)))));
            }
        }, 0);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MapActivity.this.OverLay(googleMap.addGroundOverlay(new GroundOverlayOptions().position(latLng, 2000.0f).transparency(0.5f).image(BitmapDescriptorFactory.fromBitmap(MapActivity.this.getBitmap(R.drawable.ripple_circle)))));
            }
        }, 4000);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MapActivity.this.OverLay(googleMap.addGroundOverlay(new GroundOverlayOptions().position(latLng, 2000.0f).transparency(0.5f).image(BitmapDescriptorFactory.fromBitmap(MapActivity.this.getBitmap(R.drawable.ripple_circle)))));
            }
        }, 8000);
    }

    /* access modifiers changed from: private */
    public Bitmap getBitmap(int i) {
        Drawable drawable = getResources().getDrawable(i);
        Canvas canvas = new Canvas();
        Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(createBitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    public void OverLay(final GroundOverlay groundOverlay) {
        this.vAnimator = ValueAnimator.ofInt(new int[]{0, Strategy.TTL_SECONDS_DEFAULT});
        this.vAnimator.setRepeatCount(-1);
        this.vAnimator.setDuration(12000);
        this.vAnimator.setEvaluator(new IntEvaluator());
        this.vAnimator.setInterpolator(new LinearInterpolator());
        this.vAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                valueAnimator.getAnimatedFraction();
                groundOverlay.setDimensions((float) ((Integer) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        this.vAnimator.start();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mapRipple != null && this.mapRipple.isAnimationRunning()) {
            this.mapRipple.stopRippleMapAnimation();
        }
        if (this.mGoogleApiClient != null && this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mapRipple != null && !this.mapRipple.isAnimationRunning()) {
            this.mapRipple.startRippleMapAnimation();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.mLocation = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
            if (this.mLocation != null) {
                this.mLocation.getLatitude();
                this.mLocation.getLongitude();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        this.mLocationRequest = LocationRequest.create().setPriority(100).setInterval(50).setFastestInterval(20);
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
            locationEnableDialog();
            Log.d("reque", "--->>>>");
        }
    }

    /* access modifiers changed from: private */
    public void locationEnableDialog() {
        LocationSettingsRequest.Builder addLocationRequest = new LocationSettingsRequest.Builder().addLocationRequest(this.mLocationRequest);
        addLocationRequest.setAlwaysShow(true);
        this.result = LocationServices.SettingsApi.checkLocationSettings(this.mGoogleApiClient, addLocationRequest.build());
        this.result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            public void onResult(LocationSettingsResult locationSettingsResult) {
                Status status = locationSettingsResult.getStatus();
                int statusCode = status.getStatusCode();
                if (statusCode != 0 && statusCode == 6) {
                    try {
                        status.startResolutionForResult(MapActivity.this, MapActivity.REQUEST_LOCATION);
                    } catch (IntentSender.SendIntentException unused) {
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        Log.d("onActivityResult()", Integer.toString(i2));
        if (i == REQUEST_LOCATION) {
            switch (i2) {
                case -1:
                    this.GPSEnabled = true;
                    return;
                case 0:
                    this.GPSEnabled = false;
                    CommonMethod.singleMessageDialogOKCancel(this, "Location not enabled", "You need to enabled GPS for next step.", new DialogActionListener() {
                        public void OnPositive() {
                            MapActivity.this.locationEnableDialog();
                        }

                        public void OnOnNegative() {
                            Toast.makeText(MapActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        }
                    });
                    return;
                default:
                    return;
            }
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    public void onLocationChanged(Location location) {
        this.currentLat = location.getLatitude();
        this.currentLong = location.getLongitude();
        if (!this.binding.btnNext.isEnabled()) {
            this.binding.btnNext.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            this.binding.btnNext.setEnabled(true);
        }
        Log.d(TAG, "onLocationChanged: " + this.currentLong + " / " + this.currentLat);
        float[] fArr = new float[2];
        Location.distanceBetween(this.currentLat, this.currentLong, this.circleOptions.getCenter().latitude, this.circleOptions.getCenter().longitude, fArr);
        if (((double) fArr[0]) > this.circleOptions.getRadius()) {
            Log.d(TAG, "onLocationChanged: Outside, distance from center: " + fArr[0] + " radius: " + this.circleOptions.getRadius());
        } else {
            Log.d(TAG, "onLocationChanged: Inside, distance from center: " + fArr[0] + " radius: " + this.circleOptions.getRadius());
        }
        getStateCountry(location);
    }

    private void getStateCountry(Location location) {
        try {
            List<Address> fromLocation = new Geocoder(getBaseContext(), Locale.getDefault()).getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (fromLocation.size() > 0) {
                String adminArea = fromLocation.get(0).getAdminArea();
                String countryName = fromLocation.get(0).getCountryName();
                this.stateCountry = adminArea + "#" + countryName;
                Log.d(TAG, "getStateCountry: " + adminArea + "  " + countryName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "getStateCountry: ERROR_FETCHING_LOCATION");
        }
    }

    public void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    public void onClick(View view) {
        if (view.getId() != R.id.btnNext) {
            return;
        }
        if (CommonMethod.checkInternetConnection(this)) {
            requestForSalesManLog();
        } else {
            setMsg("No Internet connection", "You are offline Please check your internet connection.");
        }
    }

    private void requestForSalesManLog() {
        if (this.mapRipple != null && this.mapRipple.isAnimationRunning()) {
            this.mapRipple.stopRippleMapAnimation();
        }
        showProgressDialog();
        String format = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        Log.d(TAG, "requestForSalesManLog: " + this.stateCountry + " , " + this.actionName + "," + this.retailerGeofenceItem.getRetailerID() + "," + format + "," + this.currentLat + "," + this.currentLong + "," + this.SMLat + "," + this.SMLong + "," + this.retailerGeofenceItem.getRetailerCode() + "," + this.MapperId);
        ((RetrofitClass) APIClient.getClient().create(RetrofitClass.class)).getSalesManLog(new RequestSManLoc(this.stateCountry, this.actionName, String.valueOf(this.retailerGeofenceItem.getRetailerID()), format, this.currentLat, this.currentLong, this.SMLat, this.SMLong, this.retailerGeofenceItem.getRetailerCode(), this.MapperId, "", "")).enqueue(new Callback<GetSalesManInfo>() {
            public void onResponse(Call<GetSalesManInfo> call, Response<GetSalesManInfo> response) {
                MapActivity.this.hideProgressDialog();
                if (response.body().getErrorNo().equals("0")) {
                    MapActivity mapActivity = MapActivity.this;
                    Toast.makeText(mapActivity, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MapActivity.this, ImageUploadActivity.class);
                    intent.putExtra("IMENo", MapActivity.this.MapperId);
                    intent.putExtra("RetailerID", String.valueOf(MapActivity.this.retailerGeofenceItem.getRetailerID()));
                    intent.putExtra("RetailerCode", MapActivity.this.retailerGeofenceItem.getRetailerCode());
                    intent.putExtra("stateCountry", MapActivity.this.stateCountry);
                    intent.putExtra("currentLat", MapActivity.this.currentLat);
                    intent.putExtra("currentLong", MapActivity.this.currentLong);
                    intent.putExtra("SMLat", MapActivity.this.SMLat);
                    intent.putExtra("SMLong", MapActivity.this.SMLong);
                    MapActivity.this.startActivity(intent);
                    return;
                }
                if (MapActivity.this.mapRipple != null && !MapActivity.this.mapRipple.isAnimationRunning()) {
                    MapActivity.this.mapRipple.startRippleMapAnimation();
                }
                MapActivity.this.setMsg("", response.body().getMsg());
            }

            public void onFailure(Call<GetSalesManInfo> call, Throwable th) {
                MapActivity.this.hideProgressDialog();
                if (MapActivity.this.mapRipple != null && !MapActivity.this.mapRipple.isAnimationRunning()) {
                    MapActivity.this.mapRipple.startRippleMapAnimation();
                }
                Log.d(MapActivity.TAG, "onFailure: " + th.getMessage());
                MapActivity.this.setMsg("", th.getMessage());
            }
        });
    }

    public void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = CommonMethod.createProgressDialog(this);
            this.mProgressDialog.setCancelable(false);
        } else if (!isFinishing() && !this.mProgressDialog.isShowing()) {
            this.mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
    }

    public void setMsg(String str, String str2) {
        CommonMethod.singleNonCancelMessageDialog(this, str, str2, new DialogActionListener() {
            public void OnOnNegative() {
            }

            public void OnPositive() {
            }
        });
    }
}
