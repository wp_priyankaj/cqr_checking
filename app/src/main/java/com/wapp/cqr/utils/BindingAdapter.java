package com.wapp.cqr.utils;

import android.view.View;
import android.widget.TextView;

public class BindingAdapter {

    @android.databinding.BindingAdapter(value={"setData"}, requireAll=true)
    public static void setData(TextView view, String path) {
        if(path==null || path.length()==0){
            //view.setText("-");
            view.setVisibility(View.GONE);
        }
        else {
            view.setVisibility(View.VISIBLE);
            view.setText(""+path);
        }
    }
}
