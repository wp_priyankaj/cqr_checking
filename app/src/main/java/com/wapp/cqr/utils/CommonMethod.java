package com.wapp.cqr.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.wapp.cqr.R;

public class CommonMethod {

    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = null;
        try {
            dialog = new ProgressDialog(mContext, R.style.MyProgressDialog);
            dialog.show();
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setProgressDrawable((new ColorDrawable(Color.GRAY)));
            dialog.setContentView(R.layout.custom_pdialog);

        } catch (WindowManager.BadTokenException e) {
            dialog = new ProgressDialog(mContext);
        } catch (Exception e) {
            dialog = new ProgressDialog(mContext);
        }
        return dialog;
    }

    public static boolean checkInternetConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("TTT", "Internet Connection Not Present");
            return false;
        }
    }

    public static void singleNonCancelMessageDialog(Activity context, String title, String msg, final DialogActionListener listener) {
        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(context).setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(context.getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.OnPositive();
            }
        });
        if (!context.isFinishing()) {

            dialog.show();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
//If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void singleMessageDialog(Context context, String title, String msg, final DialogActionListener listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.DialogTheme).setTitle(title).setMessage(msg).setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.OnPositive();
            }
        }).setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.OnOnNegative();
            }
        });

        AlertDialog dialog1 = dialog.create();
        // dialog1.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog1.show();
    }

    public static void singleMessageDialogYesNo(Activity context, String title, String msg, final DialogActionListener listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context).setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.OnPositive();
            }
        }).setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.OnOnNegative();
            }
        });

        if (!context.isFinishing()) {

            dialog.show();
        }

    }

    public static void singleMessageDialogOKCancel(Activity context, String title, String msg, final DialogActionListener listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context).setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(context.getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.OnPositive();
            }
        }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.OnOnNegative();
            }
        });

        if (!context.isFinishing()) {

            dialog.show();
        }

    }

}
