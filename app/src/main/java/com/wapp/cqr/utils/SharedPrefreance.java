package com.wapp.cqr.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.wapp.cqr.model.UserLogIn;

public class SharedPrefreance {

    String user = "User";
    String isRememberPref = "isRemember";
    SharedPreferences _pref;

    public void setUser(Context context, UserLogIn userBean) {
        _pref = context.getSharedPreferences(user, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = _pref.edit();
        editor.putString(user, new Gson().toJson(userBean));
        editor.apply();
    }

    public UserLogIn getUser(Context context) {
        _pref = context.getSharedPreferences(user, Context.MODE_PRIVATE);

        try {
            String s = _pref.getString(user, null);
            if (s == null)
                return null;
            return new Gson().fromJson(s, UserLogIn.class);
        } catch (Exception e) {
            Log.e("", "getRememberStatus: ", e.fillInStackTrace());
            return null;
        }
    }

    public void isRemember(Context context, boolean isRemember) {
        _pref = context.getSharedPreferences(isRememberPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = _pref.edit();
        editor.putBoolean(isRememberPref, isRemember);
        editor.apply();
    }

    public boolean getRememberStatus(Context context) {
        _pref = context.getSharedPreferences(isRememberPref, Context.MODE_PRIVATE);

        try {
            return _pref.getBoolean(isRememberPref, false);
        } catch (Exception e) {
            return false;
        }
    }

}

