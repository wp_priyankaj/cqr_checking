package com.wapp.cqr.utils;

public interface DialogActionListener {
    void OnPositive();
    void OnOnNegative();
}
