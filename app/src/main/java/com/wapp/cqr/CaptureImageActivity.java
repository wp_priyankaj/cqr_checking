package com.wapp.cqr;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CaptureImageActivity extends AppCompatActivity {
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String imageFilePath;
    private File image;
    private static final String TAG = "CaptureImageActivity";
    private String MapperId,RetailerID,RetailerCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_image);
        setView();
    }

    public void setView() {

        if (getIntent().getExtras() != null) {
            MapperId=getIntent().getStringExtra("IMENo");
            RetailerID=getIntent().getStringExtra("RetailerID");
            RetailerCode=getIntent().getStringExtra("RetailerCode");
            Log.d(TAG, "setView: "+RetailerID+" / "+RetailerCode+" / "+MapperId);
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_CAMERA_PERMISSION_CODE);
        } else {
            openCamera();

        }
    }

    public void openCamera() {
        Intent pictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile1();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
               // Uri photoURI = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), photoFile);

                Uri photoURI= Uri.fromFile(photoFile);
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());

                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile1() throws IOException {

        File file = new File(Environment.getExternalStorageDirectory(), "CQR");
        if (!file.exists()) {
            file.mkdir();
        }

        image = new File(file.getPath(), "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");

        imageFilePath = image.getPath();
        Log.d(TAG, "createImageFile: " + imageFilePath);
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)){
                // Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                openCamera();
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            if (image != null) {
                Log.d(TAG, "onActivityResult: RESULT_OK" +image.getPath());

                Intent i = new Intent(CaptureImageActivity.this, ImageUploadActivity.class);
                i.putExtra("CaptureFile",image.toString());
                i.putExtra("IMENo", MapperId);
                i.putExtra("RetailerID", RetailerID);
                i.putExtra("RetailerCode", RetailerCode);
                startActivity(i);
                finish();
            }
            else {
                Toast.makeText(this, "Photo capture failed", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        else if(requestCode == CAMERA_REQUEST && resultCode == RESULT_CANCELED)
        {
            finish();
        }

    }
}
