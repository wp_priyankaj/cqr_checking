package com.wapp.cqr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.wapp.cqr.databinding.ActivityLoginBinding;
import com.wapp.cqr.model.UserLogIn;
import com.wapp.cqr.restclient.APIClient;
import com.wapp.cqr.restclient.RetrofitClass;
import com.wapp.cqr.utils.CommonMethod;
import com.wapp.cqr.utils.DialogActionListener;
import com.wapp.cqr.utils.SharedPrefreance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "Login Activity";
    ActivityLoginBinding binding;
    private ProgressDialog mProgressDialog;
    boolean isRemember = false;
    private String deviceid = "357327073175247";
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    String mFirebaseToken;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        binding.remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isRemember = isChecked;
            }
        });

        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                                          PERMISSIONS_REQUEST_READ_PHONE_STATE);
                              }
                          }
            );
        } else {
            getIMEINo();
        }
        initializeFirebaseToken();


        boolean isRemember = new SharedPrefreance().getRememberStatus(this);

        if (isRemember) {
            UserLogIn dataBean = new SharedPrefreance().getUser(this);
            if (dataBean != null) {
                binding.remember.setChecked(true);
                binding.id.setText(dataBean.getAppLoginID());
                binding.password.setText(dataBean.getAppLoginPwd());
            } else {
                binding.remember.setChecked(false);

            }
        }

        binding.logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.id.getText().toString().trim().length() == 0) {
                    setMsg("", "Id is null");

                } else if (binding.password.getText().toString().trim().length() == 0) {
                    setMsg("", "password is null");

                } else {
                    userLogin();
                }
            }
        });

    }

    private void userLogin() {
        showProgressDialog();


        final UserLogIn userLogIn = new UserLogIn();
        userLogIn.setActionName("CQR_DOLOGIN");
        userLogIn.setAppLoginID(binding.id.getText().toString());
        userLogIn.setAppLoginPwd(binding.password.getText().toString());
        userLogIn.setMobileIMEINo(deviceid);
        userLogIn.setMobileInfo("MobileInfo");
        userLogIn.setVersionNo("1");
        userLogIn.setPushNotificationId("");


        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<UserLogIn> retCode = retrofitClass.userLogin(userLogIn);
        retCode.enqueue(new Callback<UserLogIn>() {
            @Override
            public void onResponse(Call<UserLogIn> call, Response<UserLogIn> response) {
                hideProgressDialog();
                if (response.body().getErrorNo().equals("0")) {
                    Toast.makeText(LoginActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();


                    new SharedPrefreance().setUser(getApplicationContext(), userLogIn);
                    new SharedPrefreance().isRemember(getApplicationContext(), isRemember);


                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else {
                    setMsg("", response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<UserLogIn> call, Throwable t) {
                hideProgressDialog();
                Log.d(TAG, "onFailure: " + t.getMessage());
                setMsg("", t.getMessage());
            }
        });

    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void getIMEINo() {
        if (CommonMethod.checkInternetConnection(LoginActivity.this)) {
            showProgressDialog();

            TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P)
                    deviceid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                else
                    deviceid = mTelephonyManager.getImei();
            } else
                deviceid = mTelephonyManager.getDeviceId();

            Log.d("msg", "DeviceImei " + deviceid);
            hideProgressDialog();

        } else {
            setMsg("No Internet connection", "You are offline Please check your internet connection.");
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initializeFirebaseToken() {

//        mFirebaseToken = FirebaseInstanceId.getInstance().getToken();

    }

    public void setMsg(String title, String msg) {
        CommonMethod.singleNonCancelMessageDialog(LoginActivity.this, title, msg, new DialogActionListener() {
            @Override
            public void OnPositive() {

            }

            @Override
            public void OnOnNegative() {

            }
        });
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = CommonMethod.createProgressDialog(LoginActivity.this);
            mProgressDialog.setCancelable(false);
        } else {
            if (!isFinishing() && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getIMEINo();
        } else {
            Toast.makeText(LoginActivity.this, "Permissions Deny", Toast.LENGTH_SHORT).show();
        }
    }

}