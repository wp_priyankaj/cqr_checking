package com.wapp.cqr;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.wapp.cqr.model.UserLogIn;
import com.wapp.cqr.utils.SharedPrefreance;

public class SplashScreenActivity extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                isUserLogin();
            }
        }, 5000);
    }

    public void isUserLogin() {

        intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}