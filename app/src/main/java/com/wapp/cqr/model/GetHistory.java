package com.wapp.cqr.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetHistory{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("VisitHistory")
	private List<VisitHistoryItem> visitHistory;

	@SerializedName("ErrorNo")
	private String errorNo;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setVisitHistory(List<VisitHistoryItem> visitHistory){
		this.visitHistory = visitHistory;
	}

	public List<VisitHistoryItem> getVisitHistory(){
		return visitHistory;
	}

	public void setErrorNo(String errorNo){
		this.errorNo = errorNo;
	}

	public String getErrorNo(){
		return errorNo;
	}
}