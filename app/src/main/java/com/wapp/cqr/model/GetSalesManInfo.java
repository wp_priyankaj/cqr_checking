package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class GetSalesManInfo{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("ErrorNo")
	private String errorNo;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setErrorNo(String errorNo){
		this.errorNo = errorNo;
	}

	public String getErrorNo(){
		return errorNo;
	}
}