package com.wapp.cqr.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetRetailerFence{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("Error")
	private String error;

	@SerializedName("RetailerGeofence")
	private List<RetailerGeofenceItem> retailerGeofence;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setError(String error){
		this.error = error;
	}

	public String getError(){
		return error;
	}

	public void setRetailerGeofence(List<RetailerGeofenceItem> retailerGeofence){
		this.retailerGeofence = retailerGeofence;
	}

	public List<RetailerGeofenceItem> getRetailerGeofence(){
		return retailerGeofence;
	}
}