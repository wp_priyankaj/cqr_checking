package com.wapp.cqr.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetRetInfo{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("RetailerId")
	private String retailerId;

	@SerializedName("RetailerDetails")
	private List<RetailerDetailsItem> retailerDetails;

	@SerializedName("ErrorNo")
	private String errorNo;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setRetailerDetails(List<RetailerDetailsItem> retailerDetails){
		this.retailerDetails = retailerDetails;
	}

	public List<RetailerDetailsItem> getRetailerDetails(){
		return retailerDetails;
	}

	public void setErrorNo(String errorNo){
		this.errorNo = errorNo;
	}

	public String getErrorNo(){
		return errorNo;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
}