package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class RequestRetCode {

    @SerializedName("ActionName")
    private String actionName;

    @SerializedName("RetailerCode")
    private String retailerCode;

    @SerializedName("MapperIMEINo")
    private String mapperIMEINo;

    @SerializedName("RetailerId")
    private String retailerId;

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setRetailerCode(String retailerCode) {
        this.retailerCode = retailerCode;
    }

    public String getRetailerCode() {
        return retailerCode;
    }

    public void setMapperIMEINo(String mapperIMEINo) {
        this.mapperIMEINo = mapperIMEINo;
    }

    public String getMapperIMEINo() {
        return mapperIMEINo;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }
}