package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class UserLogIn {
    @SerializedName("AppLoginID")
    String AppLoginID;
    @SerializedName("AppLoginPwd")
    String AppLoginPwd;
    @SerializedName("MobileIMEINo")
    String MobileIMEINo;
    @SerializedName("MobileInfo")
    String MobileInfo;
    @SerializedName("VersionNo")
    String VersionNo;
    @SerializedName("PushNotificationId")
    String PushNotificationId;
    @SerializedName("ActionName")
    private String actionName;


    @SerializedName("Msg")
    private String msg;

    @SerializedName("ErrorNo")
    private String errorNo;


    public String getAppLoginID() {
        return AppLoginID;
    }

    public void setAppLoginID(String appLoginID) {
        AppLoginID = appLoginID;
    }

    public String getAppLoginPwd() {
        return AppLoginPwd;
    }

    public void setAppLoginPwd(String appLoginPwd) {
        AppLoginPwd = appLoginPwd;
    }

    public String getMobileIMEINo() {
        return MobileIMEINo;
    }

    public void setMobileIMEINo(String mobileIMEINo) {
        MobileIMEINo = mobileIMEINo;
    }

    public String getMobileInfo() {
        return MobileInfo;
    }

    public void setMobileInfo(String mobileInfo) {
        MobileInfo = mobileInfo;
    }

    public String getVersionNo() {
        return VersionNo;
    }

    public void setVersionNo(String versionNo) {
        VersionNo = versionNo;
    }

    public String getPushNotificationId() {
        return PushNotificationId;
    }

    public void setPushNotificationId(String pushNotificationId) {
        PushNotificationId = pushNotificationId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public void setMsg(String msg){
        this.msg = msg;
    }

    public String getMsg(){
        return msg;
    }

    public void setErrorNo(String errorNo){
        this.errorNo = errorNo;
    }

    public String getErrorNo(){
        return errorNo;
    }
}
