package com.wapp.cqr.model;

import android.net.Uri;

public class ImagesModel {

    Uri uri;
    String type;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ImagesModel(Uri uri, String type) {
        this.uri = uri;
        this.type = type;
    }

    public ImagesModel() {
    }
}
