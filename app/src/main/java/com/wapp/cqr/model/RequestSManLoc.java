package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class RequestSManLoc{
	@SerializedName("ActionName")
	private String actionName;
	@SerializedName("GeoLatitude")
	private double geoLatitude;
	@SerializedName("GeoLongitude")
	private double geoLongitude;
	@SerializedName("MapperIMEINo")
	private String mapperIMEINo;
	@SerializedName("Remark")
	private String remark;
	@SerializedName("RetailerCode")
	private String retailerCode;
	@SerializedName("RetailerId")
	private String retailerId;
	@SerializedName("SMLatitude")
	private double sMLatitude;
	@SerializedName("SMLongitude")
	private double sMLongitude;
	@SerializedName("SalesData")
	private String salesData;
	@SerializedName("State_Country")
	private String stateCountry;
	@SerializedName("VisitDateTime")
	private String visitDateTime;

	public void setStateCountry(String str) {
		this.stateCountry = str;
	}

	public String getStateCountry() {
		return this.stateCountry;
	}

	public void setActionName(String str) {
		this.actionName = str;
	}

	public String getActionName() {
		return this.actionName;
	}

	public void setRetailerId(String str) {
		this.retailerId = str;
	}

	public String getRetailerId() {
		return this.retailerId;
	}

	public void setVisitDateTime(String str) {
		this.visitDateTime = str;
	}

	public String getVisitDateTime() {
		return this.visitDateTime;
	}

	public void setGeoLatitude(double d) {
		this.geoLatitude = d;
	}

	public double getGeoLatitude() {
		return this.geoLatitude;
	}

	public void setGeoLongitude(double d) {
		this.geoLongitude = d;
	}

	public double getGeoLongitude() {
		return this.geoLongitude;
	}

	public void setSMLongitude(double d) {
		this.sMLongitude = d;
	}

	public double getSMLongitude() {
		return this.sMLongitude;
	}

	public void setSMLatitude(double d) {
		this.sMLatitude = d;
	}

	public double getSMLatitude() {
		return this.sMLatitude;
	}

	public void setRetailerCode(String str) {
		this.retailerCode = str;
	}

	public String getRetailerCode() {
		return this.retailerCode;
	}

	public void setMapperIMEINo(String str) {
		this.mapperIMEINo = str;
	}

	public String getMapperIMEINo() {
		return this.mapperIMEINo;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String str) {
		this.remark = str;
	}

	public RequestSManLoc(String str, String str2, String str3, String str4, double d, double d2, double d3, double d4, String str5, String str6, String str7, String str8) {
		this.stateCountry = str;
		this.actionName = str2;
		this.retailerId = str3;
		this.visitDateTime = str4;
		this.geoLatitude = d;
		this.geoLongitude = d2;
		this.sMLongitude = d3;
		this.sMLatitude = d4;
		this.retailerCode = str5;
		this.mapperIMEINo = str6;
		this.remark = str7;
		this.salesData = str8;
	}

	public RequestSManLoc() {
	}
}