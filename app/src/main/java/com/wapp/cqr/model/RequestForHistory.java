package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class RequestForHistory{

	@SerializedName("ActionName")
	private String actionName;

	@SerializedName("MapperIMEINo")
	private String mapperIMEINo;

	public void setActionName(String actionName){
		this.actionName = actionName;
	}

	public String getActionName(){
		return actionName;
	}

	public void setMapperIMEINo(String mapperIMEINo){
		this.mapperIMEINo = mapperIMEINo;
	}

	public String getMapperIMEINo(){
		return mapperIMEINo;
	}
}