package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class RequestConfirmSManLoc {

	@SerializedName("ActionName")
	private String actionName;

	@SerializedName("statecountry")
	private String stateCountry;

	public String getStateCountry() {
		return stateCountry;
	}

	public void setStateCountry(String stateCountry) {
		this.stateCountry = stateCountry;
	}

	public RequestConfirmSManLoc(String stateCountry) {
		this.stateCountry = stateCountry;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public RequestConfirmSManLoc() {
	}
}