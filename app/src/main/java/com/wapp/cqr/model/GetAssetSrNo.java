package com.wapp.cqr.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetAssetSrNo{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("ErrorNo")
	private String errorNo;

	@SerializedName("AssetSrNos")
	private List<AssetSrNosItem> assetSrNos;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setErrorNo(String errorNo){
		this.errorNo = errorNo;
	}

	public String getErrorNo(){
		return errorNo;
	}

	public void setAssetSrNos(List<AssetSrNosItem> assetSrNos){
		this.assetSrNos = assetSrNos;
	}

	public List<AssetSrNosItem> getAssetSrNos(){
		return assetSrNos;
	}
}