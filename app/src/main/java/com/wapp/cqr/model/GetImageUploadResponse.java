package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class GetImageUploadResponse{

	@SerializedName("Msg")
	private String msg;

	@SerializedName("ReturnMobileNo")
	private String returnMobileNo;

	@SerializedName("ErrorNo")
	private String errorNo;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setReturnMobileNo(String returnMobileNo){
		this.returnMobileNo = returnMobileNo;
	}

	public String getReturnMobileNo(){
		return returnMobileNo;
	}

	public void setErrorNo(String errorNo){
		this.errorNo = errorNo;
	}

	public String getErrorNo(){
		return errorNo;
	}
}