package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class AssetSrNosItem{

	@SerializedName("AssetSrNo")
	private String assetSrNo;

	@SerializedName("AssetID")
	private int assetID;

	public void setAssetSrNo(String assetSrNo){
		this.assetSrNo = assetSrNo;
	}

	public String getAssetSrNo(){
		return assetSrNo;
	}

	public void setAssetID(int assetID){
		this.assetID = assetID;
	}

	public int getAssetID(){
		return assetID;
	}

	public AssetSrNosItem(String assetSrNo, int assetID) {
		this.assetSrNo = assetSrNo;
		this.assetID = assetID;
	}

	public AssetSrNosItem() {
	}
}