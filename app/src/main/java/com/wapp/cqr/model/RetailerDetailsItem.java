package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class RetailerDetailsItem {

    @SerializedName("Address")
    private String Address;
    @SerializedName("MobileNo")
    private String MobileNo;
    @SerializedName("OwnerName")
    private String OwnerName;
    @SerializedName("RetailerCode")
    private String RetailerCode;
    @SerializedName("RetailerID")
    private int RetailerID;
    @SerializedName("RetailerName")
    private String RetailerName;

    public int getRetailerID() {
        return this.RetailerID;
    }

    public void setRetailerID(int i) {
        this.RetailerID = i;
    }

    public String getRetailerName() {
        return this.RetailerName;
    }

    public void setRetailerName(String str) {
        this.RetailerName = str;
    }

    public String getRetailerCode() {
        return this.RetailerCode;
    }

    public void setRetailerCode(String str) {
        this.RetailerCode = str;
    }

    public String getOwnerName() {
        return this.OwnerName;
    }

    public void setOwnerName(String str) {
        this.OwnerName = str;
    }

    public String getAddress() {
        return this.Address;
    }

    public void setAddress(String str) {
        this.Address = str;
    }

    public String getMobileNo() {
        return this.MobileNo;
    }

    public void setMobileNo(String str) {
        this.MobileNo = str;
    }
}