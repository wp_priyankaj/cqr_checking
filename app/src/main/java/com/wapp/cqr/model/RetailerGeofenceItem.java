package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RetailerGeofenceItem implements Serializable {

	@SerializedName("RetailerID")
	private int retailerID;

	@SerializedName("RetailerName")
	private String retailerName;

	@SerializedName("Latitude")
	private Double  latitude;

	@SerializedName("radius")
	private int radius;

	@SerializedName("Longitude")
	private Double  longitude;

	@SerializedName("RetailerCode")
	private String retailerCode;

	public void setRetailerID(int retailerID){
		this.retailerID = retailerID;
	}

	public int getRetailerID(){
		return retailerID;
	}

	public void setRetailerName(String retailerName){
		this.retailerName = retailerName;
	}

	public String getRetailerName(){
		return retailerName;
	}


	public void setRadius(int radius){
		this.radius = radius;
	}

	public int getRadius(){
		return radius;
	}


	public void setRetailerCode(String retailerCode){
		this.retailerCode = retailerCode;
	}

	public String getRetailerCode(){
		return retailerCode;
	}

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}