package com.wapp.cqr.model;

import com.google.gson.annotations.SerializedName;

public class VisitHistoryItem {

    @SerializedName("ImageCount")
    private int imageCount;

    @SerializedName("RetailerName")
    private String retailerName;

    @SerializedName("VisitDate")
    private String visitDate;

    @SerializedName("RequestCount")
    private int requestCount;

    @SerializedName("RetailerCode")
    private String retailerCode;

    @SerializedName("DaysSinceLastVisit")
    private String daysSinceLastVisit;


    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;

    }

    public int getImageCount() {
        return imageCount;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRetailerCode(String retailerCode) {
        this.retailerCode = retailerCode;
    }

    public String getRetailerCode() {
        return retailerCode;
    }

    public String getDaysSinceLastVisit() {
        return daysSinceLastVisit;
    }

    public void setDaysSinceLastVisit(String daysSinceLastVisit) {
        this.daysSinceLastVisit = daysSinceLastVisit;
    }
}