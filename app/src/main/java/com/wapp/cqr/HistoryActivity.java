package com.wapp.cqr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.wapp.cqr.adapter.HistoryAdapter;
import com.wapp.cqr.databinding.ActivityHistoryBinding;
import com.wapp.cqr.model.GetHistory;
import com.wapp.cqr.model.GetRetailerFence;
import com.wapp.cqr.model.RequestForHistory;
import com.wapp.cqr.model.RequestRetCode;
import com.wapp.cqr.model.VisitHistoryItem;
import com.wapp.cqr.restclient.APIClient;
import com.wapp.cqr.restclient.RetrofitClass;
import com.wapp.cqr.utils.CommonMethod;
import com.wapp.cqr.utils.DialogActionListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressDialog mProgressDialog;
    private ActivityHistoryBinding binding;
    private String deviceid = "357327073175247";
    private static final String TAG = "HistoryActivity";
    private ArrayList<VisitHistoryItem> visitHistoryItemArrayList;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_history);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        setView();
    }

    public void setView() {
        setToolbar();

        if (ActivityCompat.checkSelfPermission(HistoryActivity.this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HistoryActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            getDeviceImei();
        }
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void getDeviceImei() {

        if (CommonMethod.checkInternetConnection(HistoryActivity.this)) {
            showProgressDialog();

            try {
                TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P)
                        deviceid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    else
                        deviceid = mTelephonyManager.getImei();
                } else
                    deviceid = mTelephonyManager.getDeviceId();

                Log.d("msg", "DeviceImei " + deviceid);

                if (!deviceid.equals("")) {
                    getHistory();
                } else {
                    hideProgressDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
                hideProgressDialog();
            }

        } else {
            CommonMethod.singleNonCancelMessageDialog(HistoryActivity.this, "No Internet connection", "You are offline Please check your internet connection.", new DialogActionListener() {
                @Override
                public void OnPositive() {
                    finish();
                }

                @Override
                public void OnOnNegative() {

                }
            });
        }
    }

    private void getHistory() {
//        showProgressDialog();
        RequestForHistory requestRetCode = new RequestForHistory();
        requestRetCode.setActionName("GET_VISIT_HISTORY");
        requestRetCode.setMapperIMEINo(deviceid);

        RetrofitClass retrofitClass = APIClient.getClient().create(RetrofitClass.class);
        Call<GetHistory> retCode = retrofitClass.getHistory(requestRetCode);
        retCode.enqueue(new Callback<GetHistory>() {
            @Override
            public void onResponse(Call<GetHistory> call, Response<GetHistory> response) {
                hideProgressDialog();
                try {
                    Log.d(TAG, "onResponse: " + response.body().getMsg() + "//" + response.body().getErrorNo());
                    if (response.body().getErrorNo().equals("0")) {
                        visitHistoryItemArrayList = new ArrayList<>();
                        visitHistoryItemArrayList.addAll(response.body().getVisitHistory());

                        if (visitHistoryItemArrayList.size() > 0) {
                            binding.recyclerView.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));
                            HistoryAdapter historyAdapter = new HistoryAdapter(HistoryActivity.this, visitHistoryItemArrayList);
                            binding.recyclerView.setAdapter(historyAdapter);
                        }


                    } else {
                        CommonMethod.singleNonCancelMessageDialog(HistoryActivity.this, "", response.body().getMsg() + "", new DialogActionListener() {
                            @Override
                            public void OnPositive() {
                                finish();
                            }

                            @Override
                            public void OnOnNegative() {

                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetHistory> call, Throwable t) {
                hideProgressDialog();
                Log.d(TAG, "onFailure: " + t.getMessage());
                CommonMethod.singleNonCancelMessageDialog(HistoryActivity.this, "", t.getMessage() + "", new DialogActionListener() {
                    @Override
                    public void OnPositive() {

                    }

                    @Override
                    public void OnOnNegative() {

                    }
                });
            }
        });

    }

    public void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.visit_history));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = CommonMethod.createProgressDialog(HistoryActivity.this);
            mProgressDialog.setCancelable(false);
        } else {
            if (!isFinishing() && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        } else {
            Toast.makeText(HistoryActivity.this, "Permissions Deny", Toast.LENGTH_SHORT).show();
        }
    }
}
