package com.wapp.cqr.restclient;

import com.wapp.cqr.model.GetAssetSrNo;
import com.wapp.cqr.model.GetHistory;
import com.wapp.cqr.model.GetImageUploadResponse;
import com.wapp.cqr.model.GetRetInfo;
import com.wapp.cqr.model.GetRetailerFence;
import com.wapp.cqr.model.GetSalesManInfo;
import com.wapp.cqr.model.RequestConfirmSManLoc;
import com.wapp.cqr.model.RequestForAssertNo;
import com.wapp.cqr.model.RequestForHistory;
import com.wapp.cqr.model.RequestRetCode;
import com.wapp.cqr.model.RequestSManLoc;
import com.wapp.cqr.model.UserLogIn;
import com.wapp.cqr.model.VisitHistoryItem;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetrofitClass {

    //Retailer request code
    @POST("CQRSales")
    Call<GetRetInfo> getRetInfo(@Body RequestRetCode requestRetCode);

    //Get RetailerFence
    @POST("CQRSales")
    Call<GetRetailerFence> getRetFence(@Body RequestRetCode requestRetCode);

    //Request salesman log
    @POST("CQRSales")
    Call<GetSalesManInfo> getSalesManLog(@Body RequestSManLoc requestRetCode);

    //Confirm Salesman Location
    @POST("CQRSales")
    Call<GetSalesManInfo> confirmSalesManLog(@Body RequestConfirmSManLoc requestRetCode);

    //Upload image
    @POST("CQRSales/ImageUpload")
    @Multipart
    Call<GetImageUploadResponse> uploadImages(@Query("RetailerId") String str, @Query("RetialerCode") String str2, @Query("MapperIMEINo") String str3, @Query("AssetId") String str4, @Query("AssetSrNo") String str5, @Query("Remark") String str6, @Query("SalesData") String str7, @Part MultipartBody.Part[] partArr);

    //Request get history
    @POST("CQRSales")
    Call<GetHistory> getHistory(@Body RequestForHistory requestForHistory);

    //Request for assert no
    @POST("CQRSales")
    Call<GetAssetSrNo> getAssetSrNo(@Body RequestForAssertNo requestForAssertNo);

    @POST("CQRSales")
    Call<UserLogIn> userLogin(@Body UserLogIn userLogin);

}
